import { FpNgDemoPage } from './app.po';

describe('fp-ng-demo App', () => {
  let page: FpNgDemoPage;

  beforeEach(() => {
    page = new FpNgDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
